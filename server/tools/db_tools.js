const mysql = require('mysql');

const pool = mysql.createPool({
    host: '127.0.0.1',
    user: 'root',
    port: 3306,
    password: '',
    database: 'practise',
    waitForConnections: true,
    connectionLimit: 50,
    queueLimit: 0
});

exports.querySelect = function(queryString) {
    return new Promise ((resolve, reject) => {
        try {
            pool.getConnection((err, connection) => {
                if(!!err){
                    throw err;
                }
                connection.query(queryString, (error, results, fields) => {
                    if(!!error){
                        throw error;
                    } else {
                        resolve(results);
                    }
                    pool.releaseConnection(connection);
                })
            })
        } catch (error) {
            let err = {
                message: error.message,
                name: error.name,
                stack: error.stack,
                properties: error.properties,
            };
            console.log(err)
            reject(JSON.stringify({error: err}))
        }
    })
};
