exports.assign = (urls, router, controller) => {
    urls.forEach((url) => {
        router[url.type](url.path, (req, res) => {
            let params = req.method == 'GET' ? req.params : req.body
            controller[url.method](params)
                .then(response => {
                    res.send(response)
                })
                .catch(err => {
                    console.log(err)
                    res.send(err)
                })
        })
    })
}
