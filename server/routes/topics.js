const express = require('express');
const controller = require('../controllers/topics')
const routeBuilder = require('../tools/router_builder')
const topicRouter = express.Router();

const URLS = [
    {
        path: '/insert',
        method: 'insert',
        type: 'post'
    },
    {
        path: '/update',
        method: 'update',
        type: 'post'
    },
    {
        path: '/delete',
        method: 'delete',
        type: 'post'
    },
    {
        path: '/get',
        method: 'getById',
        type: 'post'
    },
    {
        path: '/',
        method: 'getAll',
        type: 'get'
    }
];

routeBuilder.assign(URLS, topicRouter, controller)

module.exports = topicRouter;
