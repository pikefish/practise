const express = require('express');
const app = express();
const port = process.env.PORT || 3333;
const bodyParser = require('body-parser');
const router = require('./routes/index');

/**
 *
 *  TODO set and use HandleBars engine for views
 *  app.set('view engine', 'hbs');
 */

app.use(bodyParser.urlencoded({ extends: false }));
app.use(bodyParser.json());

app.use('/', router)

app.use((req, res, next) => {
    res.status(404).send('Page not found')
})

app.listen(port, () => {
    console.log(`API server started on: ${ port }. Lets ROCK!`)
})
