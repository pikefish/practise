const db = require('../tools/db_tools');
const stringifyFields = ['name', 'date', 'learningTime'];

/**
 * Метод insert предназначен для создания нового topic в БД
 * @param params
 * @param { object } params.data - данные topic
 * @param { string } params.data.name - название темы
 * @param { date } params.data.date - дата выдачи назадния
 * @param { time } params.data.learningTime - время потраченное на изучение
 * @param { number } params.data.utility - понимание изученного материала
 */
exports.insert = (params) => {
    let fields = [];
    let values = [];
    let fieldsString = '';
    let valuesString = '';
    let data = params.data
    for (let key in data) {
        fields.push(key)
        if (stringifyFields.includes(key)) {
            values.push(`'${ data[key] }'`)
        } else {
            values.push(data[key])
        }
    }
    fieldsString = `(${ fields.join(', ') })`;
    valuesString = `(${ values.join(', ') })`;
    let query =
        `
            INSERT INTO topic ${ fieldsString }
            VALUES ${ valuesString }
        `;
    return db.querySelect(query)
};

/**
 * Метод update предназначен для обновления существующего topic в БД
 * @param params
 * @param { object } params.data - данные topic
 * @param { string } params.data.name - название темы
 * @param { date } params.data.date - дата выдачи назадния
 * @param { time } params.data.learningTime - время потраченное на изучение
 * @param { number } params.data.utility - понимание изученного материала
 * @param { object | undefined } params.filter - фильтр по topic
 * @param { number | undefined} params.filter.id - id темы
 * @param { string | undefined } params.filter.name - название темы
 * @param { date | undefined } params.filter.date - дата выдачи назадния
 * @param { time | undefined } params.filter.learningTime - время потраченное на изучение
 * @param { number | undefined } params.filter.utility - понимание изученного материала
 */
exports.update = (params) => {
    let sets = [];
    let where = [];
    let setsString = '';
    let whereString = '';
    let data = params.data;
    let filter = params.filter;

    for (let key in data) {
        if (stringifyFields.includes(key)) {
            sets.push(`${ key } = '${ data[key] }'`)
        } else {
            sets.push(`${ key } = ${ data[key] }`)
        }

    }
    setsString = `${ sets.join(', ') }`;
    if (filter) {
        for (let key in filter) {
            where.push(`${ key } = ${ filter[key] }`)
        }
        whereString = `WHERE ${ where.join(' and ') }`
    }

    let query =
        `
            UPDATE topic
            SET ${ setsString }
            ${ whereString }
        `;
    return db.querySelect(query)
};

/**
 * Метод delete предназначен для удаления существующего(их) topic из БД
 * @param params
 * @param { object  | undefined } params.filter - фильтр по topic
 * @param { number  | undefined } params.filter.id - id темы
 * @param { string  | undefined } params.filter.name - название темы
 * @param { date  | undefined } params.filter.date - дата выдачи назадния
 * @param { time  | undefined } params.filter.learningTime - время потраченное на изучение
 * @param { number  | undefined } params.filter.utility - понимание изученного материала
 */
exports.delete = (params) => {
    let where = [];
    let whereString = '';
    let filter = params.filter;

    if (filter) {
        for (let key in filter) {
            where.push(`${ key } = ${ filter[key] }`)
        }
        whereString = `WHERE ${ where.join(' and ') }`
    }

    let query =
        `
            DELETE FROM topic
            ${ whereString }
        `

    return db.querySelect(query)
};

/**
 * Метод getById предназначен для получения конкретного topic из БД
 * @param { number } id - id темы
 */
exports.getById = (id) => {
    let query = `SELECT * FROM topic WHERE id = ${ id }`
    return db.querySelect(query)
};

/**
 * Метод getAll предназначен для получения всех topic из БД
 */
exports.getAll = () => {
    let query = `SELECT * FROM topic`
    return db.querySelect(query)
};
