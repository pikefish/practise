const model =  require('../models/topics');

// TODO обсудить controller

/**
 * Метод insert предназначен для создания нового topic в БД
 * @param params
 * @param { object } params.data - данные topic
 * @param { string } params.data.name - название темы
 * @param { date } params.data.date - дата выдачи назадния
 * @param { time } params.data.learningTime - время потраченное на изучение
 * @param { number } params.data.utility - понимание изученного материала
 */
exports.insert = (params) => {
    if (params && params.data) {
        return model.insert(params)
    } else {
        return Promise.reject('Empty data error')
    }
};

/**
 * Метод update предназначен для обновления существующего topic в БД
 * @param params
 * @param { object } params.data - данные topic
 * @param { string } params.data.name - название темы
 * @param { date } params.data.date - дата выдачи назадния
 * @param { time } params.data.learningTime - время потраченное на изучение
 * @param { number } params.data.utility - понимание изученного материала
 * @param { object | undefined } params.filter - фильтр по topic
 * @param { number | undefined} params.filter.id - id темы
 * @param { string | undefined } params.filter.name - название темы
 * @param { date | undefined } params.filter.date - дата выдачи назадния
 * @param { time | undefined } params.filter.learningTime - время потраченное на изучение
 * @param { number | undefined } params.filter.utility - понимание изученного материала
 */
exports.update = (params) => {
    if (params && params.data) {
        return model.update(params)
    } else {
        return Promise.reject('Empty data error')
    }
};

/**
 * Метод delete предназначен для удаления существующего(их) topic из БД
 * @param params
 * @param { object  | undefined } params.filter - фильтр по topic
 * @param { number  | undefined } params.filter.id - id темы
 * @param { string  | undefined } params.filter.name - название темы
 * @param { date  | undefined } params.filter.date - дата выдачи назадния
 * @param { time  | undefined } params.filter.learningTime - время потраченное на изучение
 * @param { number  | undefined } params.filter.utility - понимание изученного материала
 */
exports.delete = (params) => {
    return model.delete(params)
};

/**
 * Метод getById предназначен для получения конкретного topic из БД
 * @param params
 * @param { object } params.filter - фильтр по topic
 * @param { number } params.filter.id - id темы
 */
exports.getById = (params) => {
    if (params && params.filter && params.filter.id) {
        return model.getById(params.filter.id)
    } else {
        return Promise.reject('Empty data error')
    }
};

/**
 * Метод getAll предназначен для получения всех topic из БД
 */
exports.getAll = () => {
    return model.getAll()
}
